/*Queue Used to Create Huffman Tree
*Queue will have max size passed as an argument in qinit
*/

#include <inttypes.h>
#include <stdlib.h>

typedef struct qnode {
	uint8_t ch;
	uint32_t freq;
	struct qnode *left, *right;
}qnode;

typedef struct queue {
	uint32_t head, tail;
	uint32_t size;
	qnode **a;
}queue;

queue* qinit(uint32_t size);
uint32_t qempty(queue *q);
uint32_t qfull(queue *q);
void enqueue(queue *q, qnode *p);
qnode* dequeue(queue *q);
uint32_t qone(queue *q);
qnode* head(queue *q);

//Initializes Queue with max size given by the caller
queue* qinit(uint32_t size) {
	queue *q = (queue*)malloc(sizeof(queue));
	q->head = q->tail = 0;
	q->size = size;
	q->a = (qnode**)malloc(size * sizeof(qnode*));
	return q;
}

//Checks Whether Queue is empty
uint32_t qempty(queue *q) {
	return q->head == q->tail;
}

//Checks if Queue is full
uint32_t qfull(queue *q) {
	return q->tail < 0 || (q->tail == q->size && q->head == 0);
}

//Enter node at Rear i.e tail
void enqueue(queue *q, qnode *p) {
	q->a[q->tail] = p;
	if(q->tail == q->size)
		q->tail = 0;
	else if((q->head - q->tail) == 1)
		q->tail = -(q->tail);
	else
		(q->tail)++;
}

//Enter node at Front i.e head
qnode* dequeue(queue *q){
	if(q->tail < 0) {
		q->tail = -(q->tail);
		(q->tail)++;
	}
	else if(q->head == q->size) {
		q->head = 0;
	}
	(q->head)++;
	return q->a[q->head - 1];
}

//Checks if there is only 1 Element in the queue
uint32_t qone(queue *q) {
	return (q->tail - q->head) == 1 || (q->tail == 1 && q->head == q->size);
}

//Returns the Head Element of the Queue
qnode* head(queue *q) {
	if(q->head == q->size)
		return q->a[0];
	else
		return q->a[q->head];
}