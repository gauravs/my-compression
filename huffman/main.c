/*
*Implements Huffman data compression
*/
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "hcompress.c"
#include "hdecompress.c"


int main(int argc, char *argv[]) {
	
	if(argc == 3) {
		//Compression
		if(strcmp(argv[1], "c") == 0) {
			int length;
			char cname[30];
			strcpy(cname, argv[2]);
			length = strlen(argv[2]);
			strcat(cname, ".huffman");
			cname[length + 8] = '\0';
			hcompress(argv[2], cname);
		}
		//Decompression
		else if(strcmp(argv[1], "d") == 0) {
			
			int length;
			char name[30];
			strcpy(name, argv[2]);
			length = strlen(argv[2]);
			name[length - 8] = '\0';
			hdecompress(argv[2], name);
		}
		else {
			printf("<Operation> : c Compression\td Decompression");
		}
	}

	else {
		printf("<./huff> <operation> <filename>\n");
		printf("<Operation> : c Compression\td Decompression");
	}

	return 0;

}