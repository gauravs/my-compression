#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>

#include "hqueue.c"

//Huffman Node
typedef struct data {
	uint8_t ch;
	uint32_t code;
	uint32_t freq;
}data;

//Header written in huffman used for decompression
typedef struct hstruct {
	uint8_t ch;
	uint8_t str[32];
	uint16_t len;
}hstruct;

void hstore(data *a, int32_t size, hstruct *astruct);

qnode* newnode(uint8_t ch, int32_t freq);

qnode* formtree(data *a, int32_t size);

qnode* findmin(queue *q1, queue *q2);

int32_t isleaf(qnode *n);

void copya(data *a, int32_t i, int32_t top, hstruct *astruct);

void storecode(qnode *root, uint8_t x[], int32_t top, data *a, hstruct *astruct);

data *wfreq(uint32_t *size, char* input);

void writecode(char *output, hstruct *astruct, uint32_t size);

uint32_t calcpadding(data *a, hstruct *astruct, uint32_t size);

void wfile(char *input, char* output, data *a, hstruct *astruct, uint32_t size);

void msort(data *arr, uint32_t left, uint32_t right);

void merge(data a[], uint32_t l, uint32_t m, uint32_t r);

int hcompress(char *input, char *output) {

	uint32_t size;
	data *a;
	hstruct *astruct;

	a = wfreq(&size, input);
	astruct = (hstruct*)malloc(size * sizeof(hstruct));

	hstore(a, size, astruct);

	writecode(output, astruct, size);

	wfile(input, output, a, astruct, size);
	return 0;
}

#define HEIGHT 512//Height of the tree

void hstore(data *a, int32_t size, hstruct *astruct) {

	qnode *root = formtree(a, size);
	uint8_t x[HEIGHT];
	int32_t top = 0;

	storecode(root, x, top, a, astruct);
}

qnode* formtree(data *a, int32_t size) {
	int32_t i = 0;
	qnode *left, *right, *internalnode;
	queue *q1 = qinit(size);
	queue *q2 = qinit(size);
	
	for(i = 0; i < size; ++i) {
		if(!qfull(q1))
			enqueue(q1, newnode(a[i].ch, a[i].freq));
	}
	//Stops when first queue is empty and second queue contains only 1 node
	while(!(qempty(q1) && qone(q2))) {

		left = findmin(q1, q2);
		right = findmin(q1, q2);

		if(!left || !right)
			break;
		//Adding new node to the Huffman Tree
		internalnode = newnode('$', left->freq + right->freq);
		internalnode->left = left;
		internalnode->right = right;
		if(!qfull(q2))
			enqueue(q2, internalnode);
	}
	return dequeue(q2);

}

//create new node
qnode* newnode(uint8_t ch, int32_t freq) {
	qnode *temp = (qnode*)malloc(sizeof(qnode));
	temp->left = temp->right = NULL;
	temp->ch = ch;
	temp->freq = freq;
	return temp;
}

/*finds minimum freq node in Queue and Dequeue's from the Queue that has minimum
*frequency node
*/
qnode* findmin(queue *q1, queue *q2) {
	if(qempty(q1)) {
		if(!qempty(q2))
			return dequeue(q2);
		else
			return NULL;
	}

	if(qempty(q2)) {
		if(!qempty(q1))
			return dequeue(q1);
		else
			return NULL;
	}

	if(head(q1)->freq < head(q2)->freq)
		return dequeue(q1);
	return dequeue(q2);

}
/*huffman code to hstruct
*This will be the header of the compressed file
*/

void storecode(qnode *root, uint8_t x[], int32_t top, data *a, hstruct *astruct) {
	int32_t i = 0, c = top;
	/*If it traverses to the left side of tree the edge is 0
	*If it traverses to the right side of tree the edge is 1
	*Uses Recursion to traverse
	*/
	if(root->left) {
		x[top] = '0';
		storecode(root->left, x, top + 1, a, astruct);
	}
	if(root->right) {
		x[top] = '1';
		storecode(root->right, x, top + 1, a, astruct);
	}
	//Add's a Null to the array when it reaches leaf node
	if(isleaf(root)) {
		x[top] = '\0';
		while(a[i].ch != root->ch)
			i++;

		a[i].code = 0;
		//Saves the code by | with 2^k bits
		while(c) {
			c--;
			if(x[top - 1 - c] == '1')
				a[i].code |= 1 << c;
		}
		//Creates code for header of file to be compressed
		copya(a, i, top, astruct);
	}
}

//checks for leaf node
int32_t isleaf(qnode *node) {
	return !(node->left) && !(node->right);
}

void copya(data *a, int32_t i, int32_t top, hstruct *astruct) {
	int32_t c = top;
	astruct[i].ch = a[i].ch;
	astruct[i].len = c;
	while(c) {
		c--;
		astruct[i].str[top - 1 - c] = ((a[i].code >> c) & 1);
	}
}

//Reads original file and calculates frequency of characters
data *wfreq(uint32_t *size, char* input) {
	int fd;
	typedef struct character {
		uint8_t ch;
		uint32_t freq;
	}character;

	character *t = (character*)malloc(sizeof(character));
	uint8_t c;
	uint32_t i, x, n = 0;
	data *a;
	fd = open(input, O_RDONLY);
	if(fd == -1) {
		perror("File Open Failed\n");
		exit(errno);
	}
	/*
	*If the character has occured previously Frequency is incremented
	*Else character and frequency are added
	*/
	while((x = read(fd, &c, sizeof(uint8_t)))) {
		i = 0;
		while(i < n) {
			//If the Character already exists Frequency is increased
			if(c == t[i].ch) {
				t[i].freq++;
				break;
			}

			else
				i++;
		}
		//Character not found in Header Array so it will add the Character
		if(i == n) {
			t[n].ch = c;
			t[n].freq = 1;
			t = (character*)realloc(t, sizeof(character) * (n + 1) * 2);
			n++;
		}
	}
	close(fd);
	*size = n;
	a = (data*)malloc(sizeof(data) * n);
	i = 0;
	while(i < n) {
		a[i].ch = t[i].ch;
		a[i].freq = t[i].freq;
		i++;
	}
	free(t);
	msort(a, 0, n - 1);
	return a;

}

//Writes the header of the  compressed file with the codes of characters
void writecode(char *output, hstruct *astruct, uint32_t size) {
	
	uint32_t i = 0;
	int fd;
	fd = open(output, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);

	if(fd == -1) {	
		perror("File Open Failed\n");
		exit(errno);
	}

	write(fd, &size, sizeof(size));
	while(i < size) {
		write(fd, &astruct[i], sizeof(hstruct));
		i++;
	}
}

uint32_t calcpadding(data *a, hstruct *astruct, uint32_t size) {
	uint32_t i = 0, buff = 0;
	while(i < size) {
		buff += a[i].freq * astruct[i].len;
		i++;
	}
	//Number of bits that will be there in the compressed file
	buff = 32 - buff % 32;
	return buff;
}

void wfile(char *input, char* output, data *a, hstruct *astruct, uint32_t size) {
	uint32_t i, c, top;
	uint32_t pad, bitsbuff = 0, bitsinbuff;
	uint8_t character, flag = 1;
	int fr, fw;

	fr = open(input, O_RDONLY);
	if(fr == -1) {
		perror("File Open Failed\n");
		exit(errno);
	}
	fw = open(output, O_RDWR);
	if(fw == -1) {
		perror("File Open Failed\n");
		exit(errno);
	}
	//Write at the end of the header of file being compressed
	lseek(fw, 0L, 2);
	pad = calcpadding(a, astruct, size);
	//Finding padding of file to be compressed before writing data
	bitsinbuff = pad;
	write(fw, &pad, sizeof(pad));
	while(read(fr, &character, sizeof(character))) {
		i = 0;
		while(i < size) {
			if(character == astruct[i].ch) {
				break;
			}
			else
				i++;

		}

		top = astruct[i].len;
		c = 0;
		while(c != top) {
			if(astruct[i].str[c] == flag)
				bitsbuff |= (1 << (31 - bitsinbuff));
			else
				bitsbuff &= ~(1 << (31 - bitsinbuff));
			c++;
			bitsinbuff++;
			//Reset Buffer
			if(bitsinbuff == 32) {
				write(fw, &bitsbuff, sizeof(bitsbuff));
				bitsinbuff = 0;
			}

		}

	}
}

//Implements Merge Sort
void msort(data arr[], uint32_t left, uint32_t right) {
	uint32_t m;

	if(left < right) {
		m = left +  (right - left)/2;
		msort(arr, left, m);
		msort(arr, m + 1, right);
		merge(arr, left, m, right);
	}
}

void merge(data a[], uint32_t left, uint32_t m, uint32_t right) {

	uint32_t n1 = m - left + 1;
	uint32_t n2 = right - m;
	uint32_t i, j, k;

	data l[n1], r[n2];

	for(i = 0; i < n1; i++)
		l[i] = a[left + i];
	for(j = 0; j < n2; j++)
		r[j] = a[m + j + 1];

	i = 0; j = 0;
	k = left;

	while(i < n1 && j < n2) {
		if(l[i].freq <= r[j].freq) {
			a[k] = l[i];
			i++;
		}
		else {
			a[k] = r[j];
			j++;
		}
		k++;
	}

	while(i < n1) {
		a[k] = l[i];
		i++;
		k++;
	}

	while(j < n2) {
		a[k] = r[j];
		j++;
		k++;
	}
}