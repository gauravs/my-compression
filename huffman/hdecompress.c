#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>

#define T 1
#define F 0

//Header of the compressed file contains the following structure
typedef struct dhstruct {
	uint8_t ch;
	uint8_t str[32];
	uint16_t len;
}dhstruct;

dhstruct* rstruct(char *input, uint32_t *size);
uint32_t scompare(uint8_t *s, uint16_t len, dhstruct *a, uint32_t size, uint32_t *m);
void rfile(char* input, char *output, dhstruct *a, uint32_t size);
void *memorycat(void *destination, uint32_t sd, void *source, uint32_t ss);
void hdecompress(char *input, char* output);

void hdecompress(char *input, char* output) {
	uint32_t size;
	dhstruct *a;
	a = rstruct(input, &size);
	rfile(input, output, a, size);
}

/*Decompression Dictionary Table at start of the file
*Will have all the huffman codes for each character
*/
dhstruct* rstruct(char *input, uint32_t *size) {
	int fr;
	uint32_t n;
	uint32_t i;
	dhstruct *a;
	i = 0;
	fr = open(input, O_RDONLY);

	if(fr == -1) {
		perror("File Open Failed\n");
		exit(errno);
	}
	read(fr, &n, sizeof(n));
	*size = n;
	a = (dhstruct*)malloc(n * sizeof(dhstruct));

	while(i < n) {
		read(fr, &a[i], sizeof(dhstruct));
		i++;
	}

	close (fr);
	return a;
}

/*Checks for character in the header
*Returns whether the character is found or not
*/
uint32_t scompare(uint8_t *s, uint16_t len, dhstruct *a, uint32_t size, uint32_t *m) {
	uint32_t i;
	uint32_t found;

	found = F;
	i = size - 1;

	while(i > 0 && a[i].len != len) {
		i--;
	}

	while(len == a[i].len) {
		if(!memcmp(a[i].str, s, len)) {
			found = T;
			break;
		}
		else {
			if(!i)
				break;
			i--;
		}
	}
	*m = i;

	return found;
}

/*Reads compressed file
*Since Huffman is a prefixed coding method
*Whenever the code is found it decodes and writes to the output file
*/
void rfile(char* input, char *output, dhstruct *a, uint32_t size) {
	uint32_t position;
	uint32_t bitsbuff, bitsinbuff, i, bit, pad, found, z = 1;
	uint8_t s[32], one = 1, zero = 0;
	uint16_t len = 0;
	unsigned int bytesread;
	bytesread = 0;

	int fr, fw;
	
	fr = open(input, O_RDONLY);
	
	if(fr == -1) {	
		perror("File Open Failed\n");
		exit(errno);
	}

	position = sizeof(size) + size * sizeof(dhstruct);

	/*Position is set after the header of compressed file
	*Actual Reading of Compressed File Starts over here
	*/

	lseek(fr, position, SEEK_CUR);

	fw = open(output, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if(fr == -1) {	
		perror("File Open Failed\n");
		exit(errno);
	}
	//pad is the bits that are not to be read in the start of the compressed file
	read(fr, &pad, sizeof(pad));
	bitsinbuff = pad;
	while(read(fr, &bitsbuff, sizeof(bitsbuff))) {
		while(z) {

			found = F;

			while(found == F) {

				if(bitsinbuff == 32) {
					z = read(fr, &bitsbuff, sizeof(bitsbuff));
					bytesread += z;
					if(z == 0)
						break;
					bitsinbuff = 0;
				}

				bit = (bitsbuff >> (31 - bitsinbuff)) & 1;
				if(bit == 1)
					memorycat(s, len, &one, 1);
				else
					memorycat(s, len, &zero, 1);

				len++;
				found = scompare(s, len, a, size, &i);
				bitsinbuff++;
			}
			if(found == T)
				write(fw, &a[i].ch, sizeof(uint8_t));
			len = 0;
		}
	}
}

//Using memorycat as it is a binary file
void *memorycat(void *destination, uint32_t sd, void *source, uint32_t ss) {
	destination = (uint8_t*)destination + sd;
	memcpy(destination, source, ss);
	return destination;
}