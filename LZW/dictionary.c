/*
*Singly Linked Null Terminated List used as Dictionary
*/
#include <stdio.h>
#include <stdlib.h>

enum {
	empty = -1
};

typedef struct dict {

	int pos;//Position in Dictionary
	int prefix;
	int character;
	struct dict *next;

}dict;

dict *head, *tail;

void dictInit();
void appendNode(dict *node);
int dictFind(int prefix, int character);
int dictPrefix();
int dictCharacter();
void dictAdd();

void dictInit() {
	int i;
	dict *node;
	for(i = 0; i < 256; i++) {
		node = (dict*)malloc(sizeof(dict));
		node->prefix = empty;
		node->character = i;
		appendNode(node);
	}

}

void appendNode(dict *node) {
	if (head != NULL)
		tail->next = node;
	else
		head = node;
	tail = node;
	node->next = NULL;
}

//Checks Prefix + Character in Dictionary
int dictFind(int prefix, int character) {
	dict *node;
	for(node = head; node != NULL; node = node->next) {
		if((node->prefix == prefix) && (node->character == character))
			return node->pos;
	}
	return empty;
}

int dictPrefix(int pos) {
	dict *node;
	for(node = head; node != NULL; node = node->next) {
		if(node->pos == pos)
			return node->prefix;
	}
	return -1;
}

int dictCharacter(int pos) {
	dict *node;
	for(node = head; node != NULL; node = node->next) {
		if(node->pos == pos)
			return node->character;
	}
	return -1;
}

//Adds Prefix + Character to the dictionary
void dictAdd(int pos, int prefix, int character) {
	dict *node;
	node = (dict*)malloc(sizeof(dict));
	node->pos = pos;
	node->prefix = prefix;
	node->character = character;
	appendNode(node);
}

void dictDel() {
	dict *p, *q;
	p = head;
		while(p != NULL) {
			q = p;
			p = p->next;
			head = p;
			free(q);
	}
}