/*
*Splits 12 bits into 8 and 4 bits
*Writes the 8 bits of data and stores the remaining 4 bits with leftover changing it's value
*leftover is the checked and data is written accordingly
*/

#include <stdio.h>

void writeBinary(FILE *o, int code);
int readBinary(FILE *i);

int leftover = 0;
int leftoverBits;

void writeBinary(FILE *o, int code) {
    if (leftover != 0) {
        int previousCode = (leftoverBits << 4) + (code >> 8);
        
        fputc(previousCode, o);
        fputc(code, o);
        leftover = 0;
    }
    else {
        leftoverBits = code & 0xF; //save leftover, 0xF = 0000 0000 0000 0000 0000 0000 0000 1111
        leftover = 1;
        
        fputc(code >> 4, o);
    }
}

int readBinary(FILE *i) {
    int code = fgetc(i);    
    if (code == EOF)
    	return 0;

    if (leftover != 0) {
        code = (leftoverBits << 8) + code;
        
        leftover = 0;
    }
    else {
        int nextCode = fgetc(i);
        
        leftoverBits = nextCode & 0xF; //save leftover, 0xF = 0000 0000 0000 0000 0000 0000 0000 1111
        leftover = 1;
        
        code = (code << 4) + (nextCode >> 4);
    }
    return code;
}