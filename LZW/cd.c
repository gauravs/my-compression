#include <stdio.h>
#include <stdlib.h>

#include "dictionary.c"
#include "binary.c"
#include "store.c"

#define dSize 4095

void compress(FILE *r, FILE *w);
void decompress(FILE *r, FILE *w);
int decode(int code, FILE *w);

void compress(FILE *r, FILE *w) {

	int prefix = getc(r);//First Character P

	if(prefix == EOF)
		return;

	int character, index, nextcode;

	nextcode = 256;
	dictInit();//Initialize Dictionary with first 0-255 characters

	while((character = getc(r)) != EOF) {

		if((index = dictFind(prefix, character)) != -1)//P + C in dictionary?
			prefix = index;//P = P + C
		else {
			writeBinary(w, prefix);
			if (nextcode < dSize)
				dictAdd(prefix, character, nextcode++);//Add P + C to dictionary
			prefix = character;
		}
	}

	writeBinary(w, prefix);

	if(leftover > 0)
		fputc(leftoverBits << 4, w);
	dictDel();
}

void decompress(FILE *r, FILE *w) {
	int previousCode, currentCode, nextCode = 256, firstChar;

	previousCode = readBinary(r);//OLD =  First code
	if(previousCode == 0) {
		return;
	}
	fputc(previousCode, w);//Output OLD
	while ((currentCode = readBinary(r)) > 0) {//NEW = next input

		if (currentCode >= nextCode) {//NEW not in Data
			firstChar = decode(previousCode, w);//S = OLD
			fputc(firstChar, w);//Output S
		}
		else
			firstChar = decode(currentCode, w);//S = NEW

		if (nextCode < dSize)
			dataAdd(previousCode, firstChar, nextCode++);

	previousCode = currentCode;//OLD = NEW
	}

}

int decode(int code, FILE *w) {
	int character, temp;

	if(code > 255) {
		character = dataCharacter(code);
		temp = decode(dataPrefix(code), w);//Recursive Function
	}
	else {
		character = code;
		temp = code;
	}
	fputc(character, w);
	return temp;
}