typedef struct{
	int prefix;
	int character;
}data;

void dataAdd(int prefix, int character, int pos);
int dataPrefix(int pos);
int dataCharacter(int pos);

data Array[4095];

void dataAdd(int prefix, int character, int pos) {
	Array[pos].prefix = prefix;
	Array[pos].character = character;
}

int dataPrefix(int pos) {
	return Array[pos].prefix;
}

int dataCharacter(int pos) {
	return Array[pos].character;
}