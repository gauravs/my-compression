/*
*Implements LZW data compression
*/
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "cd.c"

int main(int argc, char *argv[]) {

	FILE *r; //File to be read
	FILE *w; //File to compress or decompress
	
	if(argc == 3) {
		//Compression
		if(strcmp(argv[1], "c") == 0) {
			r = fopen(argv[2], "r");
			w = fopen(strcat(argv[2], ".lzw"), "w+b");

			if(r == NULL || w == NULL) {
				perror("File could not be opened");
				return ENOENT;
			}
			compress(r, w);
		}
		//Decompression
		else if(strcmp(argv[1], "d") == 0) {
			r = fopen(argv[2], "rb");
			char name[20];
			int length = strlen(argv[2])-4;
			strncpy(name, argv[2], length);
			name[length] = '\0';
			w = fopen(name, "w");
			if(r == NULL || w == NULL) {
				perror("File could not be opened");
				return ENOENT;
			}
			decompress(r, w);
		}
		else {
			printf("<Operation> : c Compression\td Decompression");
		}
	}

	else {
		printf("<./lzw> <operation> <filename>\n");
		printf("<Operation> : c Compression\td Decompression");
	}

	fclose(r);
	fclose(w);

	return 0;

}